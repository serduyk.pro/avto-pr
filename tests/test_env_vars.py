# tests/test_env_vars.py
import os

def test_environment_variables():
    # Получаем все переменные окружения
    env_vars = os.environ

    # Проверяем, что переменные окружения не пусты
    assert env_vars is not None

    # Выводим все переменные окружения и их значения
    for var, val in env_vars.items():
        print(f"{var}: {val}")

    # Проверяем, что каждая переменная окружения имеет значение
    for var, val in env_vars.items():
        assert val is not None