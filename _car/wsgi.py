# wsgi.py
from flask import Flask
from app import app
from app.route_all import *
from app.models import *
from app.config import Config
from flask_sqlalchemy import SQLAlchemy

# Создаем экземпляр Flask приложения
app = Flask(__name__)

if __name__ == '__main__':
    # Применяем конфигурацию к приложению
    app.config.from_object(Config)
    
    # Инициализация SQLAlchemy после создания Flask-приложения
    db = SQLAlchemy(app)
    
    with app.app_context():
        
    # Создаем все таблицы в базе данных
        db.create_all()
    app.run(debug=True)