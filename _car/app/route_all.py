# app/route_all.py
from flask import render_template, redirect, url_for
from app import app

@app.route("/base")
def base():
    return render_template('base.html')

@app.route("/index")
def index():
    return render_template('index.html')

@app.route("/notification")
def notification():
    return render_template('notification.html')

@app.route("/guestion")
def guestion():
    return render_template('guestion.html')

@app.route("/viev")
def viev():
    return render_template('viev.html')