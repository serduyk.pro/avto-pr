# app/models.py
from app import db

# Модель test
class test(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    number = db.Column(db.String(15), nullable=False)
    mail = db.Column(db.String(100), nullable=True)
    service = db.Column(db.Text, nullable=False)
    dateservice = db.Column(db.DateTime, nullable=False)

    def __repr__(self):
        return f"test('{self.name}', '{self.number}', '{self.service}', '{self.dateservice}')"