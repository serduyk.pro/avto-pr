# app/__init__.py
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from dotenv import load_dotenv
from app.config import Config

load_dotenv('.env')
app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)

# Передаем объект app в route_all.py
from app import route_all