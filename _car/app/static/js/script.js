// Пример клиентского JavaScript

// Выполнить код после загрузки документа
document.addEventListener('DOMContentLoaded', function() {
    // Получить элемент по id
    var myElement = document.getElementById('myElement');

    // Добавить обработчик события клика
    myElement.addEventListener('click', function() {
        // Действие при клике на элемент
        alert('Вы кликнули на элемент!');
    });
});
