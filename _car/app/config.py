# app/config.py
import os

class Config:
    # Секретный ключ для защиты форм и сессий
    SECRET_KEY = os.getenv('SECRET_KEY', 'my_secret_key')

    # URI для подключения к базе данных
    SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URL', 'sqlite:///app.db')

    # Отключение отслеживания изменений модели для SQLAlchemy
    SQLALCHEMY_TRACK_MODIFICATIONS = os.getenv('SQLALCHEMY_TRACK_MODIFICATIONS', False)
