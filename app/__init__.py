# app/__init__.py
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from dotenv import load_dotenv
import os

def create_app():
    app = Flask(__name__)

    # Загрузка переменных окружения из файла .env
    load_dotenv()

    # Установка секретного ключа
    app.secret_key = os.getenv("SECRET_KEY")

    # Конфигурация базы данных
    app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv("DATABASE_URL")
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False  

    # Инициализация SQLAlchemy и Flask-Migrate
    db = SQLAlchemy(app)
    migrate = Migrate(app, db)
    
    # Регистрация маршрутов основных
    from .routes import main_routes
    app.register_blueprint(main_routes)
    
    # Регистрация маршрутов "регистрации на услуги"
    from .routes_rfs import rfs_routes
    app.register_blueprint(rfs_routes)
    
    return app
