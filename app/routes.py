# app/routes.py
from flask import Blueprint, render_template

main_routes = Blueprint('main', __name__)

# Страница "Главная"
@main_routes.route('/')
def index():
    return render_template('index.html')

# Страница "Услуги"
@main_routes.route('/services')
def services():
    return render_template('services.html')

# Страница "О нас"
@main_routes.route('/about')
def about():
    return render_template('about.html')

# Страница "Запись" 
# --> routes_rfs.py

# Страница "Вопросы и ответы" 
@main_routes.route("/question")
def question():
    return render_template('question.html')